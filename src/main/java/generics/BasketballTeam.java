package main.java.generics;

public class BasketballTeam implements SportTeams {
    private String teamName;

    public String getSportType() {
        return "Basketball";
    }

    public String getTeamName() {

        return teamName;
    }

    BasketballTeam(String teamName) {
        this.teamName = teamName;
    }

}
