package main.java.generics;

public class FootballTeam implements SportTeams {
    private String teamName;

    public String getTeamName() {
        return teamName;
    }

    public String getSportType() {
        return "Football";
    }

    FootballTeam(String teamName) {
        this.teamName = teamName;
    }

}
