package main.java.generics;

import java.util.*;

public class LeagueTable<T extends SportTeams> {

    private List<T> listOfTeams = new ArrayList<T>();

    void addTeam(T team) {
        listOfTeams.add(team);
    }

    void displayListOfTeams() {
        Collections.sort(listOfTeams, new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                return o1.getTeamName().compareTo(o2.getTeamName());
            }
        });
        for (T team : listOfTeams) {
            System.out.println(team.getTeamName());
        }
    }

    public static void main(String[] args) {
        LeagueTable<FootballTeam> footballLeagueTable = new LeagueTable<>();
        footballLeagueTable.addTeam(new FootballTeam("Madrid"));
        footballLeagueTable.addTeam(new FootballTeam("Yelta"));
        footballLeagueTable.addTeam(new FootballTeam("Bridge"));
        footballLeagueTable.addTeam(new FootballTeam("Siege"));
        footballLeagueTable.addTeam(new FootballTeam("Midge"));
        footballLeagueTable.displayListOfTeams();
        System.out.println("---------------------------------------------");
        LeagueTable<BasketballTeam> basketballTeamLeagueTable = new LeagueTable<>();
        basketballTeamLeagueTable.addTeam(new BasketballTeam("Nippon"));
        basketballTeamLeagueTable.addTeam(new BasketballTeam("MML"));
        basketballTeamLeagueTable.addTeam(new BasketballTeam("Messa"));
        basketballTeamLeagueTable.addTeam(new BasketballTeam("Black"));
        basketballTeamLeagueTable.displayListOfTeams();


    }
}
