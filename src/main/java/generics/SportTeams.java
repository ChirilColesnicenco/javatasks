package main.java.generics;

public interface SportTeams {
    String getTeamName();
    String getSportType();
}
